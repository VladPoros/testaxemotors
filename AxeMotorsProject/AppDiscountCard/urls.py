from django.urls import path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

router.register(r'card', views.CardDRFView)

urlpatterns = [
    path("", views.CardView.as_view(), name="card_list_url"),
    path("<int:pk>/", views.card_detail, name='card_detail_url'),
    path('search/', views.SearchCardView.as_view(), name='card_search_url'),
    path('<int:pk>/delete/', views.card_delete, name='card_delete_url'),
    path('<int:pk>/status/', views.card_status_update, name='card_status_url'),
]
