from .models import DiscountCard
from rest_framework import serializers


class DiscountCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiscountCard
        fields = '__all__'
