from django import forms
from .models import DiscountCard


class StatusForm(forms.ModelForm):
    class Meta:
        model = DiscountCard
        fields = ['status']

        widgets = {'status': forms.Select(attrs={'class': 'form-control'})}
