from django.apps import AppConfig


class AppdiscountcardConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'AppDiscountCard'
