import datetime
from django.db import models
from .CardStatuses import get_statuses
import os
from dotenv import load_dotenv

load_dotenv()


class Product(models.Model):
    # Fields
    name = models.CharField('Product', max_length=255)

    class Meta:
        db_table = "products"

    def __str__(self):
        return self.name


class DiscountCard(models.Model):
    # Fields
    id = models.AutoField(primary_key=True, verbose_name='id')
    series = models.CharField('Series card', max_length=255)
    number = models.PositiveIntegerField('Card number')
    release = models.DateField('Date release', default=datetime.date.today)
    expiration = models.DateField('Date expiration', default=datetime.date.today() + datetime.timedelta(
        days=float(os.getenv('DATE_EXPIRATION'))))
    date_using = models.DateField('Date using', default=datetime.date.today)
    amount = models.PositiveIntegerField('Amount', default=float(os.getenv('DATE_AMOUNT')))
    status = models.PositiveSmallIntegerField(choices=get_statuses())

    # Relations
    products = models.ManyToManyField(Product, blank=True)

    class Meta:
        db_table = "discount_cards"
        indexes = [
            models.Index(fields=['series']),
            models.Index(fields=['number']),
        ]
        constraints = [
            models.UniqueConstraint(fields=['series', 'number'], name='unique_series_number')
        ]

    def __str__(self):
        return self.series
