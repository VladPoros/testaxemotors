from django.core.management.base import BaseCommand
from faker import Faker
from progress.bar import Bar
from random import randrange
from datetime import timedelta, datetime
import random
from AppDiscountCard.models import Product, DiscountCard


def random_date(start, end):
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)


class Command(BaseCommand):
    help = 'Add new card'

    d1 = datetime.strptime('1/1/2019 1:30 PM', '%m/%d/%Y %I:%M %p')
    d2 = datetime.strptime('1/25/2022 4:50 AM', '%m/%d/%Y %I:%M %p')

    def add_arguments(self, parser):
        parser.add_argument('-c', '--count.cards', type=int, default=1)
        parser.add_argument('-p', '--count.products', type=int, default=1)

    def __create_products(self, count):

        fake = Faker()

        bar = Bar('Processing generate products', max=count)

        if count <= 500:
            for _ in range(count):
                product = Product()
                product.name = fake.word()
                product.save()
                bar.next()
            bar.finish()
            self.stdout.write(self.style.SUCCESS(f"Successfully generate products"))
        else:
            self.stdout.write(self.style.WARNING(f"Please input integer <= {count}"))

    def __create_cards(self, count):
        fake = Faker()

        products = list(Product.objects.all())
        random.shuffle(products)

        bar = Bar('Processing generate cards', max=count)

        if count <= 200:
            for i in range(count):
                card = DiscountCard()
                card.series = fake.lexify(letters='ABCDEFGHIJKLMNOPQRSTUVWXYZ')
                card.number = fake.random.randint(1, 100000)
                card.status = fake.random.randint(1, 2)
                card.release = random_date(self.d1, self.d2)
                card.date_using = card.release + timedelta(days=30)
                card.expiration = card.release + timedelta(days=730)
                card.save()
                card.products.set(products[:random.randint(1, 10)])
                bar.next()
            bar.finish()
            self.stdout.write(self.style.SUCCESS(f"Successfully generate cards"))

    def handle(self, *args, **options):
        self.stdout.write('===================')
        self.stdout.write(self.style.SUCCESS("Start seed database"))
        self.stdout.write('===================')

        self.__create_products(options['count.products'])
        self.__create_cards(options['count.cards'])
