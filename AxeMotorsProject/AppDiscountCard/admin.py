from django.contrib import admin

from .models import DiscountCard, Product

admin.site.register(DiscountCard)
admin.site.register(Product)
