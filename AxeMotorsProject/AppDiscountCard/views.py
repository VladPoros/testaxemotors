import datetime
from django.shortcuts import render, redirect
from django.views.generic import ListView
from .forms import StatusForm
from .models import DiscountCard
from django.db.models import Q
from datetime import timedelta
from monotonic import monotonic
from .CardStatuses import get_statuses
from django.contrib import messages

from .serializers import DiscountCardSerializer
from rest_framework import viewsets
from rest_framework.permissions import AllowAny


class CardView(ListView):
    template_name = 'card_list.html'
    context_object_name = 'latest_card_list'
    paginate_by = 10

    def get_queryset(self):
        return DiscountCard.objects.all()


class SearchCardView(ListView):
    template_name = 'card_list.html'
    context_object_name = 'latest_card_list'
    paginate_by = 10

    def get_queryset(self):
        query = self.request.GET.get('search')
        statuses = dict(get_statuses())
        status = {v.lower(): k for k, v in statuses.items()}
        if query.lower() in status.keys():
            query_status = status[query.lower()]
            find_list = DiscountCard.objects.filter(status=query_status)
        else:
            find_list = DiscountCard.objects.filter(
                Q(series__iexact=query) |
                Q(number__iexact=query) |
                Q(expiration__iexact=query)
            )
        start_time = monotonic()
        end_time = monotonic()
        print(timedelta(seconds=end_time - start_time))
        return find_list


def card_detail(request, pk):
    card = DiscountCard.objects.get(pk=pk)
    if card.expiration < datetime.date.today():
        card.status = 3
        card.save()
    form = StatusForm()
    context = {'card': card, 'form': form}
    return render(request, 'card_detail.html', context)


def card_delete(request, pk):
    card = DiscountCard.objects.get(pk=pk)
    card_clone = card
    card.delete()
    messages.add_message(request, messages.INFO, 'Card deleted - ' + card_clone.series + str(card_clone.number))

    return redirect('card_list_url')


def card_status_update(request, pk):
    card = DiscountCard.objects.get(pk=pk)
    form = StatusForm(request.POST, instance=card)
    form.save()
    messages.add_message(request, messages.INFO, 'Card status changed on ' + card.get_status_display() +
                         ' for card ' + card.series + str(card.number))
    return redirect('card_list_url')


class CardDRFView(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = DiscountCard.objects.all()
    serializer_class = DiscountCardSerializer
