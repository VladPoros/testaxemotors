Test task fort AxeMotors

Done: <br>
Create a Django web application to manage discount cards.

Field list: series,
number, release date,
expiration date, using date,
amount, status.

- Functional Applications
- Display a list of cards with fields: series, number, expiration date, activity status
- Search in the same fields
- Viewing the profile of the card with purchases on it
- Change card status
- Deleting a card
- After the expiration of the card, the status of the card is "expired"
- Added DRF (api list of cards)

For use project need:

- install requirements.txt
- use command SeedDatabase in management/commands to generate dara for database